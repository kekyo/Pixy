#!/bin/sh
m68k-elf-as -mc68000 -o test.o test.s
m68k-elf-ld -T test.ld -o test.elf test.o
m68k-elf-objcopy -S -O binary test.elf test.bin
m68k-elf-objcopy -S -O srec test.elf test.mot
m68k-elf-objcopy -S -O ihex test.elf test.hex
